﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    Rigidbody rb;
    private int count;

    public float speed;
    public Text countText;
    public Text winText;
    public Text timerText;
    float timer;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        timer = 20;
        
    }
    private void Update()
    {
        if (transform.position.y < -2f)
        {
            winText.text = "You lose! :("; 
        }
        if (timer >= 0f)
        {
            timer -= Time.deltaTime;
            SetTimerText();
        }
       
       
        if (timer <= 0f)
        {
            winText.text = "You lose! :(";
            
        }
        
        
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (timer>= 0f)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

            rb.AddForce(movement * speed);
        }
            
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            timer = timer + 1;
            SetCountText();
        }
            
    }
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 30)
        {
            winText.text = "You Win!";
        }
        if (transform.position.y < 0f)
        {
            
        }
            
    }
    void SetTimerText()
    {
        timerText.text = "Timer: " + Mathf.Round(timer).ToString();
    }

}      

