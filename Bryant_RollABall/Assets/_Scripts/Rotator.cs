﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    private Color colorStart = Color.red;
   private  Color colorEnd = Color.green;
    float duration = 1.0f;
    Renderer rend;
    private void Start()
    {
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate (new Vector3 (15, 30, 45) * Time.deltaTime);
        float lerp = Mathf.PingPong(Time.time, 5) / 5;
        rend.material.color = Color.Lerp(colorStart, colorEnd, lerp);

    }
}
